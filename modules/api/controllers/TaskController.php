<?php

namespace app\modules\api\controllers;

use app\models\Task;
use app\modules\api\dataView\task\AddTaskToSprintDataView;
use app\modules\api\dataView\task\CloseTaskDataView;
use app\modules\api\dataView\task\TaskCreateDataView;
use app\modules\api\dataView\task\EstimationDataView;
use app\modules\api\dataView\task\IndexDataView;
use app\modules\api\forms\task\AddTaskToSprintForm;
use app\modules\api\forms\task\CloseTaskForm;
use app\modules\api\forms\task\CreateEstimationForm;
use app\modules\api\forms\task\CreateTaskForm;
use app\modules\api\services\task\AddTaskToSprintService;
use app\modules\api\services\task\CloseTaskService;
use app\modules\api\services\task\CreateEstimationService;
use app\modules\api\services\task\CreateTaskService;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class TaskController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @OA\Get(
     *   path="/api/tasks",
     *   tags={"Task"},
     *   @OA\Response(
     *     response="200",
     *     description="Task",
     *     @OA\JsonContent(ref="#/components/schemas/IndexDataView")
     *   )
     * )
     */

    public function actionIndex()
    {
        $tasks = Task::find()->limit(100)->all();
        $indexDataView = new IndexDataView($tasks);
        return $this->renderJson($indexDataView->data());
    }

    /**
     * @OA\Post(
     *   path="/api/tasks",
     *   tags={"Task"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/CreateTaskForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Task create",
     *     @OA\JsonContent(ref="#/components/schemas/TaskCreateDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionCreate()
    {
        if(!Yii::$app->request->isPost)
        {
            throw new NotFoundHttpException();
        }
        $taskForm = new CreateTaskForm();
        $taskForm->load(json_decode(Yii::$app->request->getRawBody(), true));
        if($taskForm->validate())
        {
            $createTaskService = new CreateTaskService();
            $task = $createTaskService->create($taskForm);
            $dataView = new TaskCreateDataView($task);
            return $this->renderJson($dataView->taskCreateResponse());
        }
        return $this->renderJson($taskForm->getFirstErrors());
    }

    /**
     * @OA\Post(
     *   path="/api/estimate/task",
     *   tags={"Task"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/CreateEstimationForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Create Estimation time",
     *     @OA\JsonContent(ref="#/components/schemas/EstimationDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionEstimation()
    {
            if(!Yii::$app->request->isPost)
            {
                throw new NotFoundHttpException();
            }
            $estimationForm = new CreateEstimationForm();
            $estimationForm->load(json_decode(Yii::$app->request->getRawBody(), true));
            if($estimationForm->validate())
            {
                $createEstimationService = new CreateEstimationService();
                $estimation = $createEstimationService->estimate($estimationForm);
                $dataView = new EstimationDataView($estimation);
                return $this->renderJson($dataView->estimationCreateResponse());
            }
            return $this->renderJson($estimationForm->getFirstErrors());
    }

    /**
     * @OA\Post(
     *   path="/api/sprints/add-task",
     *   tags={"Task"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/AddTaskToSprintForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Add task to sprint",
     *     @OA\JsonContent(ref="#/components/schemas/AddTaskToSprintDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionAdd()
    {
        if(!Yii::$app->request->isPost)
        {
            throw new NotFoundHttpException();
        }
        $addTaskToSprintForm = new AddTaskToSprintForm();
        $addTaskToSprintForm->load(json_decode(Yii::$app->request->getRawBody(), true));
        if($addTaskToSprintForm->validate())
        {
            $addTaskToSprintService = new AddTaskToSprintService();
            $addTaskToSprint = $addTaskToSprintService->add($addTaskToSprintForm);
            $dataView = new AddTaskToSprintDataView($addTaskToSprint);
            return $this->renderJson($dataView->addTaskToSprintResponse());
        }
        return $this->renderJson($addTaskToSprintForm->getFirstErrors());
    }

    /**
     * @OA\Post(
     *   path="/api/tasks/close",
     *   tags={"Task"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/CloseTaskForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Close task",
     *     @OA\JsonContent(ref="#/components/schemas/CloseTaskDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionClose()
    {
        if(!Yii::$app->request->isPost)
        {
            throw new NotFoundHttpException();
        }
        $closeTaskForm = new CloseTaskForm();
        $closeTaskForm->load(json_decode(Yii::$app->request->getRawBody(), true));
        if($closeTaskForm->validate())
        {
            $closeTaskService = new CloseTaskService();
            $closeTask = $closeTaskService->close($closeTaskForm);
            $dataView = new CloseTaskDataView($closeTask);
            return $this->renderJson($dataView->closeTaskResponse());
        }
        return $this->renderJson($closeTaskForm->getFirstErrors());
    }

    protected function renderJson($data)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }
}