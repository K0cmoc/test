<?php

namespace app\modules\api\controllers;

use app\modules\api\dataView\sprint\CloseSprintDataView;
use app\modules\api\dataView\sprint\CreateDataView;
use app\modules\api\dataView\sprint\StartSprintDataView;
use app\modules\api\forms\sprint\CloseSprintForm;
use app\modules\api\forms\sprint\CreateSprintForm;
use app\modules\api\forms\sprint\StartSprintForm;
use app\modules\api\services\sprint\CloseSprintService;
use app\modules\api\services\sprint\CreateSprintService;
use app\modules\api\services\sprint\StartSprintService;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SprintController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @OA\Post(
     *   path="/api/sprints",
     *   tags={"Sprint"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/CreateSprintForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Sprint",
     *     @OA\JsonContent(ref="#/components/schemas/TaskCreateDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionCreate()
    {
        if(!Yii::$app->request->isPost)
        {
            throw new NotFoundHttpException();
        }
        $sprintForm = new CreateSprintForm();
        $sprintForm->load(json_decode(Yii::$app->request->getRawBody(), true));
        if($sprintForm->validate())
        {
            $createSprintService = new CreateSprintService();
            $sprint = $createSprintService->create($sprintForm);
            $dataView = new CreateDataView($sprint);
            return $this->renderJson($dataView->sprintCreateResponse());
        }
        Yii::$app->response->statusCode = 400;
        return $this->renderJson($sprintForm->getFirstErrors());
    }

    /**
     * @OA\Post(
     *   path="/api/sprints/start",
     *   tags={"Sprint"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/StartSprintForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Sprint start",
     *     @OA\JsonContent(ref="#/components/schemas/StartSprintDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionStart()
    {
        if(!Yii::$app->request->isPost)
        {
            throw new NotFoundHttpException();
        }
        $sprintForm = new StartSprintForm();
        $sprintForm->load(json_decode(Yii::$app->request->getRawBody(), true));
        if($sprintForm->validate())
        {
            $startSprintService = new StartSprintService();
            $sprint = $startSprintService->start($sprintForm);
            $dataView = new StartSprintDataView($sprint);
            return $this->renderJson($dataView->startSprintResponse());
        }
        return $this->renderJson($sprintForm->getFirstErrors());
    }

    /**
     * @OA\Post(
     *   path="/api/sprints/close",
     *   tags={"Sprint"},
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/CloseSprintForm"),
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Sprint close",
     *     @OA\JsonContent(ref="#/components/schemas/CloseSprintDataView")
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Validation Error",
     *     @OA\JsonContent(ref="#/components/schemas/response_client_errors")
     *   )
     * )
     */

    public function actionClose()
    {
        if(!Yii::$app->request->isPost)
        {
            throw new NotFoundHttpException();
        }
        $sprintForm = new CloseSprintForm();
        $sprintForm->load(json_decode(Yii::$app->request->getRawBody(), true));
        if($sprintForm->validate())
        {
            $closeSprintService = new CloseSprintService();
            $sprint = $closeSprintService->close($sprintForm);
            $dataView = new CloseSprintDataView($sprint);
            return $this->renderJson($dataView->closeSprintResponse());
        }
        return $this->renderJson($sprintForm->getFirstErrors());
    }

    protected function renderJson($data)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

}