<?php

namespace app\modules\api\dataView\sprint;

/**
 * @OA\Schema(
 *   description="Sprint create response",
 *   title="Sprint create",
 *   @OA\Property(property="Id", type="string", example="20-11"),
 * )
 */

class CreateDataView
{
    private $sprint;

    public function __construct($sprint)
    {
        $this->sprint = $sprint;
    }
    public function sprintCreateResponse()
    {
        return ['Id' => $this->sprint->sprint_number];
    }
}