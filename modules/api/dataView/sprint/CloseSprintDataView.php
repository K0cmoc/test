<?php


namespace app\modules\api\dataView\sprint;


/**
 * @OA\Schema(
 *   description="Sprint close response",
 *   title="Sprint close",
 *   @OA\Property(property="sprintId", type="string", example="20-11"),
 * )
 */

class CloseSprintDataView
{
    private $sprint;

    public function __construct($sprint)
    {
        $this->sprint = $sprint;
    }
    public function closeSprintResponse()
    {
        return ['Спринт закрыт' => $this->sprint->sprint_number];
    }

}