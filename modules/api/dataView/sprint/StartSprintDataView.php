<?php


namespace app\modules\api\dataView\sprint;


/**
 * @OA\Schema(
 *   description="Sprint start response",
 *   title="Sprint started",
 *   @OA\Property(property="sprintId", type="string", example="20-11"),
 * )
 */

class StartSprintDataView
{
    private $sprint;

    public function __construct($sprint)
    {
        $this->sprint = $sprint;
    }
    public function startSprintResponse()
    {
        return ['sprintId' => $this->sprint->sprint_number];
    }

}