<?php
namespace app\modules\api\dataView\task;

use app\models\Task;

/**
 * @OA\Schema(
 *   description="Task index",
 *   title="Tasks",
 *   @OA\Property(property="tasks", type="array",
 *     @OA\Items(
 *       @OA\Property(property="id", type="integer", example="1"),
 *       @OA\Property(property="title", type="string", example="this"),
 *       @OA\Property(property="description", type="string", example=" task is pidor"),
 *       @OA\Property(property="estimation", type="string", example="1d"),
 *       @OA\Property(property="sprint_id", type="string", example="20-10"),
 *       @OA\Property(property="status", type="string", example="open"),
 *     )
 *   )
 * )
 */

class IndexDataView
{
    /**
     * @var Task[]
     *
     */
    private $tasks;

    /**
     * EstimationDataView constructor.
     * @param array $tasks
     */
    public function __construct(array $tasks)
    {

        $this->tasks = $tasks;
    }

    public function data()
    {
        $data = [];
        foreach ($this->tasks as $task)
        {
            $data[] = [
                'id' => $task->id,
                'title' => $task->title,
                'description' => $task->description,
                'estimation' => $task->estimation,
                'sprint_id' => $task->sprint_id,
                'status' => $task->status
            ];
        }
        return $data;
    }
}