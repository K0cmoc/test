<?php

namespace app\modules\api\dataView\task;

/**
 * @OA\Schema(
 *   description="Task create response",
 *   title="Task create",
 *   @OA\Property(property="id", type="string", example="TASK-1"),
 * )
 */

class TaskCreateDataView
{
    private $task;

    public function __construct($task)
    {
        $this->task = $task;
    }

    public function taskCreateResponse()
    {
        return ['id' => 'TASK-' . $this->task->id];
    }
}
