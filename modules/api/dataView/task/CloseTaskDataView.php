<?php


namespace app\modules\api\dataView\task;


/**
 * @OA\Schema(
 *   description="Task close response",
 *   title="Task close",
 *   @OA\Property(property="task", type="string", example="Задача TASK-1 успешно закрыта"),
 * )
 */

class CloseTaskDataView
{
    private $task;

    public function __construct($task)
    {
        $this->task = $task;
    }
    public function closeTaskResponse()
    {
        return ['success' => 'Задача TASK-' . $this->task->id . ' успешно закрыта'];
}
}