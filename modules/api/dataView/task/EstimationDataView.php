<?php

namespace app\modules\api\dataView\task;

/**
 * @OA\Schema(
 *   description="Estimation time create response",
 *   title="Create Estimation time",
 *   @OA\Property(property="id", type="string", example="TASK-1"),
 * )
 */

class EstimationDataView
{
    private $estimation;

    public function __construct($estimation)
    {
        $this->estimation = $estimation;
    }

    public function estimationCreateResponse()
    {
        return ['success' => 'success', 'estimation time in seconds' => $this->estimation->estimation_seconds];
    }
}