<?php


namespace app\modules\api\dataView\task;


/**
 * @OA\Schema(
 *   description="Add task to sprint response",
 *   title="Add task to sprint",
 *   @OA\Property(property="addTaskToSprint", type="string", example="TASK-1 successfully added at sprint 1"),
 * )
 */

class AddTaskToSprintDataView
{
    private $addTaskToSprint;

    public function __construct($addTaskToSprint)
    {
        $this->addTaskToSprint = $addTaskToSprint;
    }
    public function addTaskToSprintResponse()
    {
        return ['success' => 'success', 'task' => $this->addTaskToSprint->id . " successfully added at sprint", 'sprint' => $this->addTaskToSprint->sprint_id];
    }

}