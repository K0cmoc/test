<?php


namespace app\modules\api\services\sprint;


use app\models\Sprint;
use app\modules\api\forms\sprint\StartSprintForm;

class StartSprintService
{
    public function start(StartSprintForm $startSprintForm)
    {
        $sprint = Sprint::findOne($startSprintForm->id);
        $sprint->status = 'started';
        $sprint->save(false);
        return $sprint;
    }
}