<?php


namespace app\modules\api\services\sprint;


use app\models\Sprint;
use app\modules\api\forms\sprint\CloseSprintForm;

class CloseSprintService
{
    public function close(CloseSprintForm $closeSprintForm)
    {
        $sprint = Sprint::findOne($closeSprintForm->id);
        $sprint->status = 'closed';
        $sprint->save(false);
        return $sprint;
    }
}