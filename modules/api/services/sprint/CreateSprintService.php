<?php


namespace app\modules\api\services\sprint;


use app\models\Sprint;
use app\modules\api\forms\sprint\CreateSprintForm;

class CreateSprintService
{
    public function create(CreateSprintForm $createSprintForm)
    {
        $sprint = new Sprint([
            'year' => $createSprintForm->year,
            'week' => $createSprintForm->week,
            'sprint_number' => substr($createSprintForm->year, '-2') . '-' . $createSprintForm->week,
        ]);
        $sprint->save(false);
        return $sprint;
    }
}