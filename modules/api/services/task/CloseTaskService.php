<?php


namespace app\modules\api\services\task;


use app\models\Task;
use app\modules\api\forms\task\CloseTaskForm;

class CloseTaskService
{
    public function close(CloseTaskForm $closeTaskForm)
    {
        $task = Task::findOne($closeTaskForm->id);
        $task->status = 'closed';
        $task->save(false);
        return $task;
    }

}