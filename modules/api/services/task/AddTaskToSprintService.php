<?php


namespace app\modules\api\services\task;


use app\models\Task;
use app\modules\api\forms\task\AddTaskToSprintForm;

class AddTaskToSprintService
{
    public function add(AddTaskToSprintForm $addTaskToSprintForm)
    {
        $addTaskToSprint = Task::findOne($addTaskToSprintForm->id);
        $addTaskToSprint->sprint_id = $addTaskToSprintForm->sprintId;
        $addTaskToSprint->save(false);
        return $addTaskToSprint;
    }
}