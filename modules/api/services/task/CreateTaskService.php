<?php

namespace app\modules\api\services\task;

use app\models\Task;
use app\modules\api\forms\task\CreateTaskForm;

class CreateTaskService
{
    public function create(CreateTaskForm $createTaskForm)
    {
        $task = new Task([
            'title' => $createTaskForm->title,
            'description' => $createTaskForm->description,
            'status' => $createTaskForm->status,
        ]);
        $task->save(false);
        return $task;
    }
}