<?php

namespace app\modules\api\services\task;

use app\models\Task;
use app\modules\api\forms\task\CreateEstimationForm;

class CreateEstimationService
{
    public function estimate(CreateEstimationForm $createEstimationForm)
    {
        $task = Task::findOne($createEstimationForm->id);
        $task->estimation = $createEstimationForm->estimation;
        if (strpos($task->estimation, 'w'))
        {
            $task->estimation_seconds = str_replace('w', '', $task->estimation) * 604800;
        }
        elseif (strpos($task->estimation, 'd'))
        {
            $task->estimation_seconds = str_replace('d', '', $task->estimation) * 86400;
        }
        if (strpos($task->estimation, 'h'))
        {
            $task->estimation_seconds = str_replace('h', '', $task->estimation) * 3600;
        }
        elseif (strpos($task->estimation, 'm'))
        {
            $task->estimation_seconds = str_replace('m', '', $task->estimation) * 60;
        }
        $task->save(false);
        return $task;
    }
}