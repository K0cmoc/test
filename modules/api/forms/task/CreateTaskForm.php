<?php

namespace app\modules\api\forms\task;

use yii\base\Model;

/**
 * @OA\Schema(
 *   description="Task create request",
 *   title="Task create",
 *   @OA\Property(property="title", type="string", example="Title of the task"),
 *   @OA\Property(property="description", type="string", example="Description of the task"),
 *   @OA\Property(property="status", type="string", example="open"),
 * )
 */

class CreateTaskForm extends Model
{


     public $title;
     public $description;
     public $status;

     public function rules()
     {
         return [
             [
                 'title', 'required', 'message' => 'Укажите заголовок задачи'
             ],
             [
                 'description', 'required', 'message' => 'Укажите описание задачи'
             ],
             [
                 'status', 'default', 'value' => 'open'
             ]
         ];
     }
     public function formName()
     {
         return "";
     }
}
