<?php


namespace app\modules\api\forms\task;


use app\models\Task;
use yii\base\Model;

/**
 * @OA\Schema(
 *   description="Task close request",
 *   title="Task close",
 *   @OA\Property(property="taskId", type="string", example="TASK-1"),
 * )
 */

class CloseTaskForm extends Model
{
    public $id;

    public function rules()
    {
        return [
            [
                'id', 'required', 'message' => 'Введите id задачи'
            ],
            [
                'id',
                'idValidation',
            ]
        ];
    }
    public function idValidation($attribute, $params)
    {
        if (!Task::findOne($this->{$attribute}))
        {
            $this->addError($attribute, 'Задача с таким id не найдена');
            return;
        }
    }
    public function load($data, $formName = '')
    {
        if(!parent::load($data, $formName))
        {
            return false;
        }
        $this->id = str_replace('TASK-', '', isset($data['taskId']) ? $data['taskId'] : null);
        return true;
    }

}