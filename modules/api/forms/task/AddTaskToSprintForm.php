<?php


namespace app\modules\api\forms\task;


use app\models\Sprint;
use app\models\Task;
use yii\base\Model;

/**
 * @OA\Schema(
 *   description="Add task to sprint request",
 *   title="Add task to sprint",
 *   @OA\Property(property="sprintId", type="string", example="18-20"),
 *   @OA\Property(property="taskId", type="string", example="TASK-1"),
 * )
 */

class AddTaskToSprintForm extends Model
{
    public $sprintId;
    public $id;

    public function rules()
    {
        return [
            [
                'sprintId' , 'required', 'message' => 'выберите спринт'
            ],
            [
                'sprintId',
                'sprintIdValidation',
            ],
            [
                'id', 'required', 'message' => 'выберите задачу, которую хотите добавить в спринт'
            ]
        ];
    }
    public function formName()
    {
        return "";
    }
    public function load($data, $formName = '')
    {
        if(!parent::load($data, $formName))
        {
            return false;
        }
        $this->id = str_replace('TASK-', '', isset($data['taskId']) ? $data['taskId'] : null);
        return true;
    }
    public function sprintIdValidation($attribute, $params)
    {
        if (!Sprint::findOne(['sprint_number' => $this->{$attribute}]))
        {
            $this->addError($attribute, 'Спринт с таким id не найден');
            return;
        }
        if (Task::findOne($this->id)->sprint_id == Sprint::findOne(['sprint_number' => $this->{$attribute}])->sprint_number)
        {
            $this->addError($attribute, 'Данная задача уже находится в этом спринте');
        }
        if (Task::findOne($this->id)->sprint_id)
        {
            $this->addError($attribute, 'Данная задача уже находится в другом спринте');
        }

    }
}