<?php

namespace app\modules\api\forms\task;


use app\models\Task;
use yii\base\Model;

/**
 * @OA\Schema(
 *   description="Estimation time create request",
 *   title="Create Estimation time",
 *   @OA\Property(property="id", type="string", example="TASK-1"),
 *   @OA\Property(property="estimation", type="string", example="1h"),
 * )
 */

class CreateEstimationForm extends Model
{
    public $id;
    public $estimation;

    public function rules()
    {
        return [
            [
                'id', 'required', 'message' => 'Введите id задачи'
            ],
            [
                'estimation', 'required', 'message' => 'Введите оценочное время выполнения задачи'
            ],
            [
                'id',
                'idValidation',
            ],
        ];
    }
    public function idValidation($attribute, $params)
    {
        if (!Task::findOne($this->{$attribute}))
        {
            $this->addError($attribute, 'Задача с таким id не найдена');
            return;
        }
    }
    public function load($data, $formName = '')
    {
        if(!parent::load($data, $formName))
        {
            return false;
        }
        $this->id = str_replace('TASK-', '', isset($data['id']) ? $data['id'] : null);
        return true;
    }
    public function formName()
    {
        return "";
    }
}