<?php


namespace app\modules\api\forms\sprint;


use app\models\Sprint;
use yii\base\Model;

/**
 * @OA\Schema(
 *   description="Sprint close request",
 *   title="Sprint close",
 *   @OA\Property(property="id", type="integer", example="1"),
 * )
 */

class CloseSprintForm extends Model
{
    public $id;

    public function rules()
    {
        return [
            [
                'id', 'required', 'message' => 'Введите id спринта'
            ],
            [
                'id',
                'idValidation',
            ]
        ];
    }
    public function idValidation($attribute, $params)
    {
        $sprint = Sprint::findOne($this->{$attribute});
        if (!$sprint)
        {
            $this->addError($attribute, 'Спринт с таким id не найден');
            return;
        }
        if ($sprint->status == null)
        {
            $this->addError($attribute, 'Спринт еще не был запущен');
        }
        if ($sprint->getTasks()->where(['like', 'status', 'open'])->exists())
        {
            $this->addError($attribute, 'В спринте остались еще не закрытые задачи');
        }
        if ($sprint->status == 'closed')
        {
            $this->addError($attribute, 'Спринт уже закрыт');
        }
    }
    public function load($data, $formName = '')
    {
        if(!parent::load($data, $formName))
        {
            return false;
        }
        $sprint = Sprint::findOne(['sprint_number' => isset($data['sprintId']) ? $data['sprintId'] : null]);
        if(!$sprint)
        {
            return false;
        }
        $this->id = $sprint->id;
        return true;
    }
}