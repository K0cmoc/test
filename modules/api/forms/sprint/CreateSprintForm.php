<?php

namespace app\modules\api\forms\sprint;

use yii\base\Model;

/**
 * @OA\Schema(
 *   description="Sprint create request",
 *   title="Sprint create",
 *   @OA\Property(property="Week", type="integer", example="11"),
 *   @OA\Property(property="Year", type="integer", example="2020")
 * )
 */

class CreateSprintForm extends Model
{
    public $week;
    public $year;

    public function rules()
    {
        return [
            [
                'week',
                'required',
                'message' => 'укажите номер недели'
            ],
            [
                'year',
                'required',
                'message' => 'укажите год'
            ],
            [
                'week',
                'integer',
                'max' => 52,
                'min' => 1,
                'message' => 'Номер недели не может быть меньше 1 и больше 52',
            ],
            [
                'week', 'unique', 'targetClass' => 'app\models\Sprint', 'message' => 'Для данной недели уже сущетсвует спринт'
            ],
        ];
    }
    public function formName()
    {
        return "";
    }
    public function load($data, $formName = '')
    {
        if(!parent::load($data, $formName))
        {
            return false;
        }
        $this->week = isset($data['Week']) ? $data['Week'] : null;
        $this->year = isset($data['Year']) ? $data['Year'] : null;

        return true;
    }
}