<?php


namespace app\modules\api\forms\sprint;


use app\models\Sprint;
use yii\base\Model;


/**
 * @OA\Schema(
 *   description="Sprint start request",
 *   title="Sprint started",
 *   @OA\Property(property="id", type="integer", example="1"),
 * )
 */

class StartSprintForm extends Model
{
    public $id;

    public function rules()
    {
        return [
            [
              'id', 'required', 'message' => 'Введите id спринта'
            ],
            [
                'id',
                'idValidation',
            ]
        ];
    }
    public function idValidation($attribute, $params)
    {
        $sprint = Sprint::findOne($this->{$attribute});
        if (!$sprint)
        {
            $this->addError($attribute, 'Спринт с таким id не найден');
            return;
        }
        if ($sprint->status == 'started')
        {
            $this->addError($attribute, 'Спринт уже запущен');
        }
        if ($sprint->getTasks()->count() == 0)
        {
            $this->addError($attribute, 'Добавьте задачи в спринт');
        }
        if ($sprint->getTasks()->where(['is', 'estimation', null])->exists())
        {
            $this->addError('error ', 'Задачу ' . $sprint->getTasks()->where(['is', 'estimation', null])->one()->id . ' сначала нужно оценить');
        }
        if(Sprint::find()->where(['status' => 'started'])->exists())
        {
            $this->addError($attribute, 'У Вас уже есть запущенный спринт, сперва необходимо закончить его');
        }
        $secondsInOneWeek = 604800;
        if (strtotime($sprint->year . '-W' . $sprint->week . '-0')-strtotime('now') >= $secondsInOneWeek)
        {
            $this->addError($attribute, 'До начала этого спринта больше 7 дней, его нельзя запускать');
        }
        $tasks = $sprint->getTasks()->all();
        $totalTime = 0;
        foreach ($tasks as $task)
        {
            $totalTime += $task->estimation_seconds;
        }
        $fortyHours = 144000;
        if ($totalTime > $fortyHours)
        {
            $this->addError($attribute, 'Общее время выполнения задач в данном спринте превышает 40 часов, уменьшите число задач');
        }
    }
    public function load($data, $formName = '')
    {
        if(!parent::load($data, $formName))
        {
            return false;
        }
        $sprint = Sprint::findOne(['sprint_number' => isset($data['sprintId']) ? $data['sprintId'] : null]);
        if(!$sprint)
        {
            return false;
        }
        $this->id = $sprint->id;
        return true;
    }
}