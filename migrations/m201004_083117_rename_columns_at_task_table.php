<?php

use yii\db\Migration;

/**
 * Class m201004_083117_rename_columns_at_task_table
 */
class m201004_083117_rename_columns_at_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%task}}', 'estimation_unix', 'estimation_seconds');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201004_083117_rename_columns_at_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201004_083117_rename_columns_at_task_table cannot be reverted.\n";

        return false;
    }
    */
}
