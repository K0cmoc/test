<?php

use yii\db\Migration;

/**
 * Class m201002_045239_change_sprint_table
 */
class m201002_045239_change_sprint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%sprint}}', 'year');
        $this->dropColumn('{{%sprint}}', 'week');
        $this->addColumn('{{%sprint}}', 'sprint_number', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_045239_change_sprint_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_045239_change_sprint_table cannot be reverted.\n";

        return false;
    }
    */
}
