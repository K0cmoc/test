<?php

use yii\db\Migration;

/**
 * Class m201001_141719_add_taskId_column_in_estimation_table
 */
class m201001_141719_add_taskId_column_in_estimation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%estimation}}', 'taskId', $this->string()->notNull()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201001_141719_add_taskId_column_in_estimation_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201001_141719_add_taskId_column_in_estimation_table cannot be reverted.\n";

        return false;
    }
    */
}
