<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sprint}}`.
 */
class m201002_041759_create_sprint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sprint}}', [
            'id' => $this->primaryKey(),
            'year' => $this->integer()->notNull(),
            'week' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sprint}}');
    }
}
