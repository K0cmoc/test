<?php

use yii\db\Migration;

/**
 * Class m201004_075123_add_columns_at_sprint_table
 */
class m201004_075123_add_columns_at_sprint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sprint}}', 'week', $this->integer()->after('id'));
        $this->addColumn('{{%sprint}}', 'year', $this->integer()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201004_075123_add_columns_at_sprint_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201004_075123_add_columns_at_sprint_table cannot be reverted.\n";

        return false;
    }
    */
}
