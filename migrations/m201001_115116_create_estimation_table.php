<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%estimation}}`.
 */
class m201001_115116_create_estimation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%estimation}}', [
            'id' => $this->primaryKey(),
            'estimation' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%estimation}}');
    }
}
