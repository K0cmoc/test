<?php

use yii\db\Migration;

/**
 * Class m201002_075511_add_columns_at_sprint_table
 */
class m201002_075511_add_columns_at_sprint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sprint', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_075511_add_columns_at_sprint_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_075511_add_columns_at_sprint_table cannot be reverted.\n";

        return false;
    }
    */
}
