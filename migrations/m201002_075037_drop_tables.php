<?php

use yii\db\Migration;

/**
 * Class m201002_075037_drop_tables
 */
class m201002_075037_drop_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%add_task}}');
        $this->dropTable('{{%estimation}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_075037_drop_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_075037_drop_tables cannot be reverted.\n";

        return false;
    }
    */
}
