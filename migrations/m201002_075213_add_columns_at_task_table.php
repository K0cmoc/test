<?php

use yii\db\Migration;

/**
 * Class m201002_075213_add_columns_at_task_table
 */
class m201002_075213_add_columns_at_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'estimation', $this->string());
        $this->addColumn('task', 'sprint_id', $this->string());
        $this->addColumn('task', 'status', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_075213_add_columns_at_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_075213_add_columns_at_task_table cannot be reverted.\n";

        return false;
    }
    */
}
