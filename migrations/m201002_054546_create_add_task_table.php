<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%add_task}}`.
 */
class m201002_054546_create_add_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%add_task}}', [
            'id' => $this->primaryKey(),
            'sprintId' => $this->string()->notNull(),
            'taskId' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%add_task}}');
    }
}
