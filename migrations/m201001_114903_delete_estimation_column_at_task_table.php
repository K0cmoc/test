<?php

use yii\db\Migration;

/**
 * Class m201001_114903_delete_estimation_column_at_task_table
 */
class m201001_114903_delete_estimation_column_at_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%task}}', 'estimation');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201001_114903_delete_estimation_column_at_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201001_114903_delete_estimation_column_at_task_table cannot be reverted.\n";

        return false;
    }
    */
}
