<?php


namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Sprint
 * @package app\models
 * @property int id;
 * @property int year;
 * @property int week;
 * @property string sprint_number;
 * @property string status;
 */

class Sprint extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%sprint}}';
    }
    public function getTasks()
    {
        return $this->hasMany(Task::class, ['sprint_id' => 'sprint_number']);
    }
}