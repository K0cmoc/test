<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Task
 * @package app\models
 * @property int id;
 * @property string title;
 * @property string description;
 * @property string estimation;
 * @property int estimation_seconds;
 * @property int sprint_id;
 * @property string status;
 */

class Task extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%task}}';
    }
    public function getSprint()
    {
        return $this->hasOne(Sprint::class, ['sprint_number' => 'sprint_id']);
    }
}
